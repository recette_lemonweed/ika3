module.exports = {
	name: "Img",
	triggers: ["img"],
	description: "Image search using Startpage (Google) or DuckDuckGo. Defaults to Startpage.",
	category: "search",
	arguments: {
		positional: ["terms"],
		flags: {
			"family-filter": [false, "f"],
			random: [false, "r"],
			backend: {
				duckduckgo: [false, "ddg", "d"],
				startpage: [false, "sp", "s"],
				brave: [false, "b"]
			}
		}
	},
	func: func
};

let pageBase = "http://duckduckgo.com/?ia=images&iax=images&k5=1&q=";
let jsonBase = "https://duckduckgo.com/i.js?l=us-en&o=json&vqd=VQD&f=,,,&p=-1&v7exp=a&q=";
let Request = require("request");
let HTMLParse = require("node-html-parser").parse;

function duckduckgo(message, string, args){
	let query = encodeURIComponent(string);
	let url = pageBase+query+(args.f ? "" : "&kp=-2");
	Request.get(url, {}, (perr, pres, pbod) => {
		if(perr || !pbod)
			throw("nope");

		let vqd = pbod.match(/&vqd=(.+)&p/)[1];
		let url = jsonBase.replace("VQD", vqd)+query;

		if(args.f)
			url = url.replace("p=-1", "p=1");

		Utility.get(url, (jerr, jres, jbod) => {
			let results = JSON.parse(jbod).results;

			if(!results[0])
				return message.reply("Nothing found.");

			let index = args.random ? (Math.random() * results.length)|0 : 0;
			let embed = new Discord.MessageEmbed({
				title: results[index].title,
				author: {
					name: results[index].url.split("/")[2],
					url: results[index].url
				},
				image: {
					url: results[index].image
				},
				color: Config.embedColour,
				footer: {
					text: (index+1)+" of "+results.length + " | " + results[index].width+"x"+results[index].height
				}
			});

			message.channel.send({embed}).then(mes=>{
				let controls = new Utility.MessageControls(mes, message.author);

				controls.on("reaction", r => {
					if(r.n === 0 && results[index-1])
						index--;
					else if(r.n === 1 && results[index+1])
						index++;
					else return;

					embed.author.name = results[index].url.split("/")[2];
					embed.author.url = results[index].url;
					embed.image.url = results[index].image;
					embed.title = results[index].title;
					embed.footer.text = (index+1)+" of "+results.length + " | " + results[index].width+"x"+results[index].height;

					mes.edit({embed});
				});
			});
		});
	});
}


let j = Request.jar();
j.setCookie(Request.cookie('preferences=disable_family_filterEEE1N1Ndisable_open_in_new_windowEEE0N1Ndisable_video_family_filterEEE1N1Nenable_post_methodEEE1N1Nenable_proxy_safety_suggestEEE1N1Nenable_stay_controlEEE0N1Ngeo_mapEEE0N1Nlang_homepageEEEs/default/en/N1NlanguageEEEenglishN1Nlanguage_uiEEEenglishN1Nnum_of_resultsEEE10N1Nother_iaEEE0N1NsuggestionsEEE0N1Nwikipedia_iaEEE0N1Nwt_unitEEEcelsius'), "https://startpage.com");

function startpage(message, string, args){
	let jar = args.f ? undefined : j;
	Request.post("https://startpage.com/sp/search", {
		jar: jar,
		headers: {
			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
			"Accept-Encoding": "identity" // no gzip thx
		},
		form: {
			"language": "english",
			"lui": "english",
			"query": string,
			"cat": "pics"
		}
	}, (err, res, bod) => {
		if(err || !bod)
			return message.reply("Something went wrong.");
		let results = bod.match(/(?<=data-img-metadata=')(.+)(?=')/g);
		if(!results)
			return message.reply("No results.");
		results = results.map(JSON.parse);

		let index = args.random ? (Math.random() * results.length)|0 : 0;
		let embed = new Discord.MessageEmbed({
			title: HTMLParse(results[index].title).text,
			author: {
				name: results[index].displayUrl.split("/")[2],
				url: results[index].displayUrl
			},
			image: {
				url: results[index].clickUrl
			},
			color: Config.embedColour,
			footer: {
				text: (index+1)+" of "+results.length + " | " + results[index].width+"x"+results[index].height
			}
		});

		message.channel.send({embed}).then(mes=>{
			let controls = new Utility.MessageControls(mes, message.author);

			controls.on("reaction", r => {
				if(r.n === 0 && results[index-1])
					index--;
				else if(r.n === 1 && results[index+1])
					index++;
				else return;

				embed.author.name = results[index].displayUrl.split("/")[2];
				embed.author.url = results[index].displayUrl;
				embed.image.url = results[index].clickUrl;
				embed.title = HTMLParse(results[index].title).text;
				embed.footer.text = (index+1)+" of "+results.length + " | " + results[index].width+"x"+results[index].height;

				mes.edit({embed});
			});
		});
	});
}

function brave(message, string, args){
	let j = Request.jar();
	j.setCookie(Request.cookie("safesearch=off"), "https://search.brave.com");
	let jar = args.f ? undefined : j;
	let headers = {
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
		"Accept-Encoding": "identity", // no gzip thx
		"Accept-Language": "en",
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/jxl,image/webp,*/*;q=0.8"
	};
	Request.get("https://search.brave.com/images?q=" + encodeURIComponent(string) + "&source=web", {
		headers, jar,
		}, (err, res, bod) => {
			if(!bod || err)
				return message.reply("Ok, something didn't work.");
			Request.get("https://search.brave.com/api/images?q=" + encodeURIComponent(string) + "&source=web", {
				headers, jar,
			}, (err, res, bod) => {
				if(!bod || err)
					return message.reply("Ok, API didn't work.");
				let results;
				try{
					results = JSON.parse(bod).results;
				}catch(err){
					return message.reply("Ok, parsing JSON didn't work.");
				}
				function res(i){
					return new Discord.MessageEmbed({
						title: results[i].title,
						author: {
							name: results[i].source.replace(/^\w/, l => l.toUpperCase()),
							url: results[i].url
						},
						image: {
							url: results[i].properties.url
						},
						color: results[i].thumbnail.bg_color,
						footer: {
							text: (i+1)+" of "+results.length + " | " + results[i].properties.width+"x"+results[i].properties.height
						}
					});
				}
				message.channel.send({embed: res(0)}).then(mes=>{
					let controls = new Utility.MessageControls(mes, message.author);
					let index = args.random ? (Math.random() * results.length)|0 : 0;

					controls.on("reaction", r => {
						if(r.n === 0 && results[index-1])
							index--;
						else if(r.n === 1 && results[index+1])
							index++;
						else return;
						mes.edit({embed: res(index)});
					});
				});
			});
		}
	);
}

let backends = {duckduckgo, startpage, brave};

function func(message, args){
	let string = args._.join(" ");
	if(!string)
		return message.reply("Gonna need something to search for.");

	backends[args.backend || "startpage"](message, string, args);
}
